import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import {UserProvider} from "./UserContext";

import AppNavbar from "./components/AppNavbar";

import Courses from "./pages/Courses";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import CourseView from "./pages/CourseView";

import {Container} from "react-bootstrap";
import './App.css';

function App() {
    const [user, setUser] = useState({
      // email: localStorage.getItem("email")

      id: null, 
      isAdmin: null
    })

    console.log(user);

    const  unsetUser = () =>{
      localStorage.clear();
    }

    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user]);


    useEffect(() => {
      fetch("http://localhost:4000/users/details", {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data =>{
      console.log(data);

      if(typeof data._id !== "undeined"){
      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })
      }
      else{
      setUser({

        id: null,
        isAdmin: null

      })
    }
  })
    }, [])

  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      <AppNavbar />
      <Container fluid>
        <Routes>
          <Route exact path ="/" element={<Home />} />
          <Route exact path ="/courses" element={<Courses />} />
          <Route exact path ="/courseView" element={<CourseView />} />
          <Route exact path ="/register" element={<Register />} />
          <Route exact path ="/login" element={<Login />} />
          <Route exact path ="/logout" element={<Logout />} />
          <Route exact path ="*" element={<Error />} />
        </Routes>
      </Container>
      </Router>
    </UserProvider>
  );
}

/*
  Mini Activity

  1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the CourseCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;
