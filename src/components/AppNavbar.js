// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";
import {Link} from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext";


import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

  const{user} = useContext(UserContext);
    // const [user, setUser] = useState(localStorage.getItem("email"));
    // console.log(user);

  return(
    <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/" >Zuitt</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
          {/*className is use instead of "class", to specify a CSS classes*/}
            <Nav className="ms-auto" defaultActiveKey= "/">
              <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/courses" eventKey="/Courses">Course</Nav.Link>
             {
                (user.id !== null)
                ?
                    <Nav.Link as={Link} to="/logout" eventKey="/Logout">Logout</Nav.Link>
                :
                  <>
                    <Nav.Link as={Link} to="/login" eventKey="/Login">Login</Nav.Link>
                    <Nav.Link as={Link} to="/register" eventKey="/Register">Register</Nav.Link>
                  </>
             }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}