import {useContext, useEffect} from "react";
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";

export default function Logout(){

 	// localStorage.clear();

    const {setUser, unsetUser} = useContext(UserContext);

    unsetUser();
    // console.Log(user);

    useEffect(() =>{
        setUser({
            id: null,
            isAdmin: null
        })
    })

 	return(

 			<Navigate to="/login" />

 		)
 }