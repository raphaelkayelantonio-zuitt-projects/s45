import coursesData from "../data/coursesData";
import {useEffect, useState} from "react";
import CourseCard from "../components/CourseCard";

export default function Courses(){

	const [courses, setCourses] = useState([]);

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCourses(data.map(course =>{
				return(
					<CourseCard key={course.id} courseProp={course} />
				)
			}))
		})
	},[])

	//Check if we can access the mock database.

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// Props
		// is a shorthand for propertty since components are considered as object in ReactJS.
		// Props is a way to pass data from the parent to child component.
		// it is synonymous to the function parameter.

		//map method
		// const courses = coursesData.map(course => {
		// 	return(
		// 		<CourseCard key={course.id} courseProp={course} />
		// 	)
		// })

	return(
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}