import Banner from "../components/Banner";
import Highlights from "../components/Highlights";


export default function home(){
	
	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Opputunities for everyone, everywhere.",
		destination: "/courses",
		label: "Enroll"
	}


	return(
		<>
			<Banner data={data} />
     		<Highlights />
		</>
	)
}